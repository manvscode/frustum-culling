/*
 *	FrustumCulling.cc
 * 
 *	
 *
 *	Coded by Joseph A. Marrero
 *	2/25/2007
 */
#include <cassert>
#include <iostream>
#include "FrustumCulling.h"
#include <GL/glut.h>

using namespace std;

int main( int argc, char *argv[] )
{
	glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE );
	glutInitWindowSize( 640, 480 );
	
	glutCreateWindow( "FrustumCulling" );
	glutFullScreen( );

	glutDisplayFunc( render );
	glutReshapeFunc( resize );
	glutKeyboardFunc( keyboard_keypress );
	glutIdleFunc( idle );

	initialize( );
	glutMainLoop( );
	deinitialize( );

	return 0;
}


void initialize( )
{
	glEnable( GL_ALPHA_TEST );
	
	glEnable( GL_POINT_SMOOTH );
	glEnable( GL_LINE_SMOOTH );
	glEnable( GL_POLYGON_SMOOTH );
	glHint( GL_POINT_SMOOTH_HINT, GL_NICEST );
	glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
	glHint( GL_POLYGON_SMOOTH_HINT, GL_NICEST );
	glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );
	
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	

	glShadeModel( GL_SMOOTH );
	glClearDepth( 1.0f );		
	glDepthFunc( GL_LEQUAL );
	glEnable( GL_DEPTH_TEST );


	glPixelStorei( GL_PACK_ALIGNMENT, 4 );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 4 );


	// TO DO: Initialization code goes here...
}

void deinitialize( )
{
	// TO DO: Deinitialization code goes here...
}


void render( )
{
	glClearColor( 0.1f, 0.1f, 0.1f, 0.1f );
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity( );
	

	// TO DO: Drawing code goes here...




	/* Write text */
	int width = glutGet((GLenum)GLUT_WINDOW_WIDTH);
	int height = glutGet((GLenum)GLUT_WINDOW_HEIGHT);

	writeText( GLUT_BITMAP_HELVETICA_18, std::string("Frustum Culling"), 2, 22 );
	writeText( GLUT_BITMAP_9_BY_15, std::string("Press Q to quit."), 2, 5 );
	
	glutSwapBuffers( );
}

void resize( int width, int height )
{
	glViewport( 0, 0, width, height );
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity( );
	

	if( height == 0 )
		height = 1;
		
	gluPerspective( 45.0f, (double) width / (double) height, 1.0, 50.0 );
	glMatrixMode( GL_MODELVIEW );
}

void keyboard_keypress( unsigned char key, int x, int y )
{
	switch( key )
	{
		case 'Q':
		case 'q':
		case GLUT_KEY_F1:
		case GLUT_KEY_END:
			deinitialize( );
			exit( 0 );
			break;
		default:
			break;
	}

}

void idle( )
{ glutPostRedisplay( ); }

void writeText( void *font, std::string &text, int x, int y )
{
	int width = glutGet( (GLenum) GLUT_WINDOW_WIDTH );
	int height = glutGet( (GLenum) GLUT_WINDOW_HEIGHT );

	glDisable( GL_DEPTH_TEST );
	glDisable( GL_TEXTURE_2D );
	glDisable( GL_LIGHTING );

	glMatrixMode( GL_PROJECTION );
	glPushMatrix( );
		glLoadIdentity( );	
		glOrtho( 0, width, 0, height, 1.0, 10.0 );
			
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix( );
			glLoadIdentity( );
			glColor3f( 1.0f, 1.0f, 1.0f );
			glTranslatef( 0.0f, 0.0f, -1.0f );
			glRasterPos2i( x, y );

			for( unsigned int i = 0; i < text.size( ); i++ )
				glutBitmapCharacter( font, text[ i ] );
			
		glPopMatrix( );
		glMatrixMode( GL_PROJECTION );
	glPopMatrix( );
	glMatrixMode( GL_MODELVIEW );

	glEnable( GL_LIGHTING );
	glEnable( GL_DEPTH_TEST );
	glEnable( GL_TEXTURE_2D );

}
