#ifndef _FRUSTUMCULLING_H_
#define _FRUSTUMCULLING_H_
/*
 *	FrustumCulling.h
 *
 *	
 *
 *	Coded by Joseph A. Marrero
 *	2/25/2007
 */
#include <string>
using namespace std;


void initialize( );
void deinitialize( );

void render( );
void resize( int width, int height );
void keyboard_keypress( unsigned char key, int x, int y );
void idle( );
void writeText( void *font, std::string &text, int x, int y );


#endif
