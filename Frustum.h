#ifndef _FRUSTUM_H_
#define _FRUSTUM_H_

class CFrustum
{
  public:
	typedef enum tagPlane {
		LEFT,
		RIGHT,
		BOTTOM,
		TOP,
		BACK,
		FRONT
	} Plane;

	enum {
		A, B, C, D
	};

	CFrustum( );
	~CFrustum( );
	void calculateFrustum( bool normalize = true );
	bool isPointInFrustum( float x, float y, float z );
	bool isSphereInFrustum( float x, float y, float z, float radius );
	bool isCubeInFrustum( float x, float y, float z, float size );

  private:
	void normalize( Plane p );

	float m_Frustum[ 6 ][ 4 ];
};

#endif